module mesh_partitioning
  implicit none
  
  private
  public :: partition_mesh
  
contains
  subroutine partition_mesh( no_nodes, CSR_elem_list, CSR_node_list, no_parts, elem_partition, node_partition, &
                             min_common_vtx_ngbrs, node_weights, node_sizes, partition_weights, edgecut_or_comm_vol)
    use metis_interfaces, only: METIS_part_mesh_dual, &
                                METIS_part_mesh_nodal, &
                                check_metis_output
    use, intrinsic :: iso_c_binding, only: c_double, c_int, c_loc, c_null_ptr, c_ptr
    
    integer, intent(in) :: no_nodes
    integer, dimension(:), intent(in) :: CSR_elem_list
    integer, dimension(:), intent(in) :: CSR_node_list
    integer, intent(in) :: no_parts
    integer, allocatable, dimension(:), intent(out) :: elem_partition
    integer, allocatable, dimension(:), intent(out) :: node_partition
    integer, optional, intent(in) :: min_common_vtx_ngbrs
    integer, dimension(no_nodes), optional, intent(in) :: node_weights
    integer, dimension(no_nodes), optional, intent(in) :: node_sizes
    real, dimension(no_parts), optional, intent(in) :: partition_weights
    integer, optional, intent(out) :: edgecut_or_comm_vol
    
    integer :: no_elem
    
    integer(c_int), target :: c_no_elem
    integer(c_int), target :: c_no_nodes
    integer(c_int), dimension(size(CSR_elem_list)), target :: c_CSR_elem_list
    integer(c_int), dimension(size(CSR_node_list)), target :: c_CSR_node_list
    integer(c_int), target :: c_min_common_vtx_ngbrs
    integer(c_int), target :: c_no_parts
    integer(c_int), target :: c_edgecut_or_comm_vol
    integer(c_int), allocatable, dimension(:), target :: c_elem_partition
    integer(c_int), dimension(size(node_partition)), target :: c_node_partition
    integer(c_int), allocatable, dimension(:), target :: c_node_weights
    integer(c_int), allocatable, dimension(:), target :: c_node_sizes
    real(c_double), allocatable, dimension(:), target :: c_partition_weights
    
    type(c_ptr) :: ne
    type(c_ptr) :: nn
    type(c_ptr) :: eptr
    type(c_ptr) :: eind
    type(c_ptr) :: vwgt
    type(c_ptr) :: vsize
    type(c_ptr) :: ncommon
    type(c_ptr) :: nparts
    type(c_ptr) :: tpwgts
    type(c_ptr) :: options
    type(c_ptr) :: objval
    type(c_ptr) :: epart
    type(c_ptr) :: npart
    integer(c_int) :: metis_out
    
    no_elem = size(CSR_elem_list) - 1
    c_no_elem = no_elem
    ne = c_loc(c_no_elem)
    c_no_nodes = no_nodes
    nn = c_loc(c_no_nodes)
    c_CSR_elem_list = CSR_elem_list
    eptr = c_loc(c_CSR_elem_list)
    c_CSR_node_list = CSR_node_list
    eind = c_loc(c_CSR_node_list)
    if (present(node_weights)) then
      allocate(c_node_weights(size(node_weights)))
      c_node_weights = node_weights
      vwgt = c_loc(c_node_weights)
    else
      vwgt = c_null_ptr
    endif
    if (present(node_sizes)) then
      allocate(c_node_sizes(size(node_sizes)))
      c_node_sizes = node_sizes
      vsize = c_loc(c_node_sizes)
    else
      vsize = c_null_ptr
    endif
    c_no_parts = no_parts
    nparts = c_loc(c_no_parts)
    if (present(min_common_vtx_ngbrs)) then
      c_min_common_vtx_ngbrs = min_common_vtx_ngbrs
      ncommon = c_loc(c_min_common_vtx_ngbrs)
    endif
    if (present(partition_weights)) then
      allocate(c_partition_weights(size(partition_weights)))
      c_partition_weights = partition_weights
      tpwgts = c_loc(c_partition_weights)
    else
      tpwgts = c_null_ptr
    endif
    options = c_null_ptr
    objval = c_loc(c_edgecut_or_comm_vol)
    allocate(c_elem_partition(no_elem))
    epart = c_loc(c_elem_partition)
    npart = c_loc(c_node_partition)
    
    if (present(min_common_vtx_ngbrs)) then
      metis_out = METIS_part_mesh_dual(ne,nn,eptr,eind,vwgt,vsize,ncommon,nparts,tpwgts,options,objval,epart,npart)
    else
      metis_out = METIS_part_mesh_nodal(ne,nn,eptr,eind,vwgt,vsize,nparts,tpwgts,options,objval,epart,npart)
    endif
    call check_metis_output(metis_out)
    
    if (present(edgecut_or_comm_vol)) then
      edgecut_or_comm_vol = c_edgecut_or_comm_vol
    endif
    allocate(elem_partition(no_elem))
    elem_partition = c_elem_partition
    allocate(node_partition(no_nodes))
    node_partition = c_node_partition
  end subroutine partition_mesh
end module mesh_partitioning

