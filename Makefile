FC = ifort
CC = gcc
EXE = RUN

METIS_DIR = /home/aldohennink/METIS_INSTALL/
METIS_LIB = -L${METIS_DIR}lib/ -lmetis
METIS_FFLAGS = -I ${METIS_DIR}include/


FFLAGS = -warn all -check all
# The program will error if I don't leave out the stack check:
FFLAGS += -check nostack

OBJS = \
main.o \
mesh_partitioning.o

all : $(EXE)

$(EXE) : $(OBJS)
	$(FC) -o $(EXE) $(OBJS) metis_interfaces.o $(METIS_LIB)

$(OBJS):
	$(FC) $(FFLAGS) -c $(@:.o=.f90) -o $@ -module .

METIS/metis.o:
	$(CC) $(METIS_FFLAGS) -c METIS/metis.c -o $@

metis_interfaces.o:
	$(FC) -c $(@:.o=.f90) -o $@ -module .

clean:
	rm -f $(EXE) *.mod *.o METIS/metis.o


$(OBJS): Makefile

main.o: \
  main.f90 \
  mesh_partitioning.o
mesh_partitioning.o: \
  mesh_partitioning.f90 \
  metis_interfaces.o
METIS/metis.o: \
  METIS/metis.c
metis_interfaces.o: \
  metis_interfaces.f90 \
  METIS/metis.o


