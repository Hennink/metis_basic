module metis_interfaces
  use, intrinsic :: iso_c_binding, only: c_int
  implicit none
  
  private
  public :: METIS_part_mesh_dual
  public :: METIS_part_mesh_nodal
  public :: check_metis_output
  
  ! These values are copied from 'metis.h'. I don't know how to let Fortran
  ! extract it from the header file.
  integer(c_int), parameter :: METIS_OK              = 1
  integer(c_int), parameter :: METIS_ERROR_INPUT     = -2
  integer(c_int), parameter :: METIS_ERROR_MEMORY    = -3
  integer(c_int), parameter :: METIS_ERROR           = -4
  
  interface
    function METIS_part_mesh_dual( ne,nn,eptr,eind,vwgt,vsize,ncommon, &
                                    nparts,tpwgts,options,objval,      &
                                    epart,npart )                      &
                                  bind(c,name='METIS_PartMeshDual')
      use, intrinsic :: iso_c_binding, only: c_int, c_ptr
      implicit none
      
      type(c_ptr), value :: ne
      type(c_ptr), value :: nn
      type(c_ptr), value :: eptr
      type(c_ptr), value :: eind
      type(c_ptr), value :: vwgt
      type(c_ptr), value :: vsize
      type(c_ptr), value :: ncommon
      type(c_ptr), value :: nparts
      type(c_ptr), value :: tpwgts
      type(c_ptr), value :: options
      type(c_ptr), value :: objval
      type(c_ptr), value :: epart
      type(c_ptr), value :: npart
      integer(c_int) :: METIS_part_mesh_dual
    end function METIS_part_mesh_dual
    
    function METIS_part_mesh_nodal( ne,nn,eptr,eind,vwgt,vsize,       &
                                    nparts,tpwgts,options,objval,     &
                                    epart,npart )                     &
                                  bind(c,name='METIS_PartMeshNodal')
      use, intrinsic :: iso_c_binding, only: c_int, c_ptr
      implicit none
      
      type(c_ptr), value :: ne
      type(c_ptr), value :: nn
      type(c_ptr), value :: eptr
      type(c_ptr), value :: eind
      type(c_ptr), value :: vwgt
      type(c_ptr), value :: vsize
      type(c_ptr), value :: nparts
      type(c_ptr), value :: tpwgts
      type(c_ptr), value :: options
      type(c_ptr), value :: objval
      type(c_ptr), value :: epart
      type(c_ptr), value :: npart
      integer(c_int) :: METIS_part_mesh_nodal
    end function METIS_part_mesh_nodal
  end interface
  
contains
  subroutine check_metis_output(metis_output)
    use, intrinsic :: iso_c_binding, only: c_int
    
    integer(c_int), intent(in) :: metis_output
    
    select case(metis_output)
      case(METIS_OK)
        
      case(METIS_ERROR_INPUT)
        stop 'METIS error: erroneous inputs and/or options'
      case(METIS_ERROR_MEMORY)
        stop 'METIS error: insufficient memory'
      case(METIS_ERROR)
        stop 'METIS error: unknown error'
      case default
        stop 'METIS error: unknown output value'
    end select
  end subroutine check_metis_output
end module metis_interfaces

