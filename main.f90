program main
  use mesh_partitioning, only: partition_mesh
  
  integer, parameter :: no_nodes_per_elem = 4
    
  integer :: no_elem
  integer :: no_nodes
  integer, allocatable, dimension(:) :: CSR_elem_list
  integer, allocatable, dimension(:) :: CSR_node_list
  integer :: no_parts
  integer, allocatable, dimension(:) :: elem_partition
  integer, allocatable, dimension(:) :: node_partition
    
  ! Element 1 has nodes 0 1 2 3
  ! Element 2 has nodes 1 4 5 2
  no_elem = 2
  no_nodes = 6
  allocate(CSR_elem_list(no_elem+1))
  allocate(CSR_node_list(no_elem*no_nodes_per_elem))
  CSR_elem_list = [0,4,8]
  CSR_node_list = [0,1,2,3,1,4,5,2]
  no_parts = 2
  
  call partition_mesh(no_nodes,CSR_elem_list,CSR_node_list,no_parts,elem_partition,node_partition)
  print *, elem_partition
  print *, node_partition
end program main

